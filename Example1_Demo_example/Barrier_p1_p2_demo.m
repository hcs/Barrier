% File demonstrate computation of barrier certificate and probability
% bounds using sum-of square programing for Example 3 in paper 
% "P. Jagtap, S. Soudjani, and M. Zamani, Temporal Logic Verification 
% of Stochastic Systems using Barrier Certificates, submitted to ATVA 2018
% 
% Example: Lorenz model for a thermal convection loop
%
% Requirements: to run this code user needs to install 
%       1] SOSTOOLS (download: http://www.cds.caltech.edu/sostools/) 
%       2] SeDuMi (download: http://sedumi.ie.lehigh.edu/)
% Created on: 3 May 2018
% Author: Pushpak Jagtap
% Tested on: MATLAB R2017a on Windows PC
%
% NOTE: One can easily modify generate_prog.m file for simulating other 
% examples in the paper.  
%
% following code do bisection method to compute papameters $gamma$ and $c$  
% in the SOS program which gives reachability probability as $gamma+N*c$ 
% =============================================

% =============================================
% Initial geuss for gamma and c (Selected from intiution to reduce
% computation time but one can always use 1)
function [cc,gg,bb]=Barrier_p1_p2_demo()
clear all;
c_final=0; gamma_final=0;
ga=0.1; ca=0.5; 
gap=0; cap=0; zz=ca; yy=ga;  % temporary variables

% bisection for computing gamma and c
while((abs(ca-cap)>=0.0005)) %0.001 is a precision (stopping criteria for bisecion)
        [prog, V] =generate_prog_p1_p2_demo(ga,ca); 
        % =============================================
        % get solution
        SOLV = sosgetsol(prog,V);   
        % =============================================
        % checking if solution is feasible or not 
        [c2,m2] = coeffs(SOLV);
        pp = length(c2);
        zz=zz-zz/2;
        if pp==0
            cap=ca;
            ca=ca+zz;
        end
        if pp~=0
            c_final=ca;
            cap=ca;
            ca=ca-zz;
        end
end
%%
ca=c_final;
while((abs(ga-gap)>=0.001)) %0.001 is a precision (stopping criteria for bisecion
        [prog, V] =generate_prog_p1_p2_demo(ga,ca);   
        % =============================================
        % get solution
        SOLV = sosgetsol(prog,V); 
        % =============================================
        % checking if solution is feasible or not 
        [c2,m2] = coeffs(SOLV);
        pp = length(c2);
        yy=yy-yy/2;
        if pp==0
            gap=ga;
            ga=ga+yy;
        end  
        if pp~=0
            gamma_final=ga;
            gap=ga;
            ga=ga-yy;
            Barrier_certificate=SOLV;
        end
end

% ===================================
% Printing final values for parameters and barrier certificate  
cc=c_final;
gg=gamma_final;
bb=Barrier_certificate;

% Output:
%
% c_final = 0.00098
% gamma_final = 0.0016
% Barrier_certificate =
% - 1.853e-12*x1^5 + 8.823e-7*x1^4*x2 + 1.233e-5*x1^4 - 1.467e-6*x1^3*x2^2
% - 5.958e-6*x1^3*x2 - 3.892e-5*x1^3 + 1.373e-7*x1^2*x2^3 + 1.596e-5*x1^2*x2^2 
% + 2.674e-6*x1^2*x2 + 3.969e-5*x1^2 - 2.664e-7*x1*x2^4 + 2.261e-6*x1*x2^3 
% - 1.71e-5*x1*x2^2 + 6.036e-6*x1*x2 - 1.218e-5*x1 - 1.459e-12*x2^5 
% + 1.776e-6*x2^4 - 3.706e-6*x2^3 + 5.309e-6*x2^2 - 1.848e-6*x2 + 1.294e-6

% Elapsed time is 410.541037 seconds.



