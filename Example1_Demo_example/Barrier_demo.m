% File demonstrate computation of barrier certificate and probability
% bounds using sum-of square programing for Example 2 in paper 
% "P. Jagtap, S. Soudjani, and M. Zamani, Temporal Logic Verification 
% of Stochastic Systems using Barrier Certificates, submitted to ATVA 2018
% 
% Example 1: Demo Example
%
% Requirements: to run this code user needs to install 
%       1] SOSTOOLS (download: http://www.cds.caltech.edu/sostools/) 
%       2] SeDuMi (download: http://sedumi.ie.lehigh.edu/)
% Created on: 3 May 2018
% Author: Pushpak Jagtap
% Tested on: MATLAB R2017a on Windows PC

clc;
clear all;
tic;
% computation of barreir certificates
[c1,gamma1, barrier1]=Barrier_p0_p1_demo();
[c2,gamma2, barrier2]=Barrier_p1_p2_demo();
[c3,gamma3, barrier3]=Barrier_p2_p1_demo();
[c4,gamma4, barrier4]=Barrier_p0_p2_demo();
% computing lower bound on probabilities  
p=1-((gamma1+3*c1)*(gamma2+3*c2)+(gamma3+4*c3)+(gamma4+3*c4)*(gamma3+3*c3))
toc;