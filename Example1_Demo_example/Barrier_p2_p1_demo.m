% File demonstrate computation of barrier certificate and probability
% bounds using sum-of square programing for Example 3 in paper 
% "P. Jagtap, S. Soudjani, and M. Zamani, Temporal Logic Verification 
% of Stochastic Systems using Barrier Certificates, submitted to ATVA 2018
% 
% Example: Lorenz model for a thermal convection loop
%
% Requirements: to run this code user needs to install 
%       1] SOSTOOLS (download: http://www.cds.caltech.edu/sostools/) 
%       2] SeDuMi (download: http://sedumi.ie.lehigh.edu/)
% Created on: 3 May 2018
% Author: Pushpak Jagtap
% Tested on: MATLAB R2017a on Windows PC
%
% NOTE: One can easily modify generate_prog.m file for simulating other 
% examples in the paper.  
%
% following code do bisection method to compute papameters $gamma$ and $c$  
% in the SOS program which gives reachability probability as $gamma+N*c$ 
% =============================================

% =============================================
% Initial geuss for gamma and c (Selected from intiution to reduce
% computation time but one can always use 1)
function [cc,gg,bb]=Barrier_p2_p1_demo()
clear all;
c_final=0; gamma_final=0;
ga=0.5; ca=0.5; 
gap=0; cap=0; zz=ca; yy=ga;  % temporary variables

% bisection for computing gamma and c
while((abs(ca-cap)>=0.0005)) %0.001 is a precision (stopping criteria for bisecion)
        [prog, V] =generate_prog_p2_p1_demo(ga,ca); 
        % =============================================
        % get solution
        SOLV = sosgetsol(prog,V);   
        % =============================================
        % checking if solution is feasible or not 
        [c2,m2] = coeffs(SOLV);
        pp = length(c2);
        zz=zz-zz/2;
        if pp==0
            cap=ca;
            ca=ca+zz;
        end
        if pp~=0
            c_final=ca;
            cap=ca;
            ca=ca-zz;
        end
end
%%
ca=c_final;
while((abs(ga-gap)>=0.001)) %0.001 is a precision (stopping criteria for bisecion
        [prog, V] =generate_prog_p2_p1_demo(ga,ca);   
        % =============================================
        % get solution
        SOLV = sosgetsol(prog,V); 
        % =============================================
        % checking if solution is feasible or not 
        [c2,m2] = coeffs(SOLV);
        pp = length(c2);
        yy=yy-yy/2;
        if pp==0
            gap=ga;
            ga=ga+yy;
        end  
        if pp~=0
            gamma_final=ga;
            gap=ga;
            ga=ga-yy;
            Barrier_certificate=SOLV;
        end
end

% ===================================
% Printing final values for parameters and barrier certificate  
cc=c_final
gg=gamma_final
bb=Barrier_certificate

% Output:
%
% c_final = 0.00125
% gamma_final = 0.0016
% Barrier_certificate =
% - 1.08e-5*x1^4 - 3.098e-6*x1^3*x2 - 2.294e-6*x1^3*x3 + 8.674e-7*x1^3 
% + 1.058e-5*x1^2*x2^2 + 2.862e-6*x1^2*x2*x3 - 3.683e-6*x1^2*x2 
% + 3.538e-5*x1^2*x3^2 - 0.0002381*x1^2*x3 - 0.01013*x1^2 - 1.224e-6*x1*x2^3 
% + 3.7e-7*x1*x2^2*x3 - 1.785e-6*x1*x2^2 - 1.555e-6*x1*x2*x3^2 
% + 3.289e-5*x1*x2*x3 + 5.319e-5*x1*x2 + 3.442e-6*x1*x3^3 + 8.905e-8*x1*x3^2 
% + 3.365e-6*x1*x3 + 2.86e-5*x1 - 4.347e-6*x2^4 - 2.353e-7*x2^3*x3 
% + 6.704e-7*x2^3 + 5.211e-5*x2^2*x3^2 - 0.0001704*x2^2*x3 - 0.01*x2^2 
% - 5.678e-7*x2*x3^3 + 1.787e-6*x2*x3^2 - 4.891e-5*x2*x3 + 4.998e-5*x2 
% + 6.27e-6*x3^4 + 0.0004961*x3^3 + 0.05897*x3^2 - 0.7921*x3 + 3.586



