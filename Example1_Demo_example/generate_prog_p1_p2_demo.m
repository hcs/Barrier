% File demonstrate computation of barrier certificate and probability
% bounds using sum-of square programing for Example 1 in paper 
% "P. Jagtap, S. Soudjani, and M. Zamani, Temporal Logic Verification 
% of Stochastic Systems using Barrier Certificates, submitted to ATVA 2018
% 
% Example: Demo Example
%
% Requirements: to run this code user needs to install 
%       1] SOSTOOLS (download: http://www.cds.caltech.edu/sostools/) 
%       2] SeDuMi (download: http://sedumi.ie.lehigh.edu/)
% Created on: 3 May 2018
% Author: Pushpak Jagtap
% Tested on: MATLAB R2017a on Windows PC
%
% NOTE: One can easily modify generate_prog.m file for simulating other 
% examples in the paper.  

function [y,z]=generate_prog_p1_p2_demo(ga,ca)
        % =============================================
        % Define symbolic variables states and noise signals 
        syms x1 x2 w1 w2 ;
        % =============================================
        % Define variables required for SOS program 
        vars = [x1; x2];
        
        % =============================================
        % Define Dynamics x(k+1)=f(x(k),w(k)) here
        f = [x1-0.01*x2*x2+0.1*w1;
            x2-0.01*x1*x2+0.1*w2];

        % =============================================
        % Define regions of interest as sospolynomials 
        g0= [x1+10; (x2+10)*-x2; -x1-x2];  %p0
        g1 = [x1*(10-x1); (10-x2); x1+x2];  %p1
        g2 = [-x1*(10+x1); x2*(10-x2)];  %p2
        
        % =============================================
        %  Initialization the sum of squares program
        prog = sosprogram(vars);
        
        %  Defining monomials for barrier certificates and lagrange multiplier 
        VEC = monomials(vars,[0:5]);
        VEC1 = monomials(vars,[0:2]);
        % =============================================
        % The Barrier function V: 
        [prog,V] = sospolyvar(prog,VEC,'wscoeff');

        % =============================================
        % Computing E[V(x(k+1) | x(k)]
        VV = subs(V,{x1,x2},[f(1),f(2)]);
        [c,m] = coeffs(VV);
        for k=1:length(m)
            s1 = strsplit(char(m(k)),'*');
            chk = 0;
            for k2=1:length(s1)
                s2 = strsplit(char(s1(k2)),'^');
                if (strcmp(s2(1),'w1')&& (length(s2)==1)) || (strcmp(s2(1),'w2')&& (length(s2)==1))
                    m(k) = 0 ;
                    chk = 1;
                    break
                end
                if (strcmp(s2(1),'w1') || strcmp(s2(1),'w2')) && mod(str2num(char(s2(2))),2)==1
                    m(k) = 0;
                    chk = 1;
                    break;
                end
                if (strcmp(s2(1),'w1') || strcmp(s2(1),'w2'))
                    dfk = str2num(char(s2(2)))/2;
                    df = (2^dfk)*factorial(dfk);
                    s1{k2} = num2str(df);
                end
            end
            if chk == 0
              sy1 = sym(s1);
              m(k) = prod(sy1);
            end
        end
        po2 = sum(c.*m);
        
        % =============================================
        % Lagrange multipliers
        [prog,V] = sospolyvar(prog,VEC,'wscoeff');
        [prog,V1] = sospolyvar(prog,VEC,'wscoeff');
        [prog,V2] = sospolyvar(prog,VEC,'wscoeff');
        [prog,V3] = sospolyvar(prog,VEC,'wscoeff');
        [prog,V4] = sospolyvar(prog,VEC,'wscoeff');
        [prog,V5] = sospolyvar(prog,VEC,'wscoeff');

        % =============================================
        % Next, define SOSP constraints as mentioned in Lemma 3 in paper[1]
        prog = sosineq(prog,V1);
        prog = sosineq(prog,V2);
        prog = sosineq(prog,V3);
        prog = sosineq(prog,V4);
        prog = sosineq(prog,V5);
        prog = sosineq(prog,V);
        
        prog = sosineq(prog,-V-[V1 V2 V3]*g1+ga);
        prog = sosineq(prog,V-[V4 V5]*g2-1);
        prog = sosineq(prog,-po2+V+ca);  

        % =============================================
        % And call sos solver (requires SeDuMi installed!)
        y = sossolve(prog);
        z=V;
end