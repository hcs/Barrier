% File demonstrate computation of barrier certificate and probability
% bounds using sum-of square programing for Example 3 in paper 
% "P. Jagtap, S. Soudjani, and M. Zamani, Temporal Logic Verification 
% of Stochastic Systems using Barrier Certificates, submitted to ATVA 2018
% 
% Example: Lorenz model for a thermal convection loop
%
% Requirements: to run this code user needs to install 
%       1] SOSTOOLS (download: http://www.cds.caltech.edu/sostools/) 
%       2] SeDuMi (download: http://sedumi.ie.lehigh.edu/)
% Created on: 3 May 2018
% Author: Pushpak Jagtap
% Tested on: MATLAB R2017a on Windows PC
%
% NOTE: One can easily modify generate_prog.m file for simulating other 
% examples in the paper.  
%
% following code do bisection method to compute papameters $gamma$ and $c$  
% in the SOS program which gives reachability probability as $gamma+N*c$ 
% =============================================

% =============================================
% Initial geuss for gamma and c (Selected from intiution to reduce
% computation time but one can always use 1)
function [cc,gg,bb]=Barrier_p0_p1_demo()
clear all;
c_final=0; gamma_final=0;
ga=1; ca=1; 
gap=0; cap=0; zz=ca; yy=ga;  % temporary variables

% bisection for computing gamma and c
while((abs(ca-cap)>=0.0001)) %0.001 is a precision (stopping criteria for bisecion)
        [prog, V] =generate_prog_p0_p1_demo(ga,ca); 
        % =============================================
        % get solution
        SOLV = sosgetsol(prog,V);   
        % =============================================
        % checking if solution is feasible or not 
        [c2,m2] = coeffs(SOLV);
        pp = length(c2);
        zz=zz-zz/2;
        if pp==0
            cap=ca;
            ca=ca+zz;
        end
        if pp~=0
            c_final=ca;
            cap=ca;
            ca=ca-zz;
        end
end
%%
ca=c_final;
while((abs(ga-gap)>=0.001)) %0.001 is a precision (stopping criteria for bisecion
        [prog,V] =generate_prog_p0_p1_demo(ga,ca);   
        % =============================================
        % get solution
        SOLV = sosgetsol(prog,V); 
        % =============================================
        % checking if solution is feasible or not 
        [c2,m2] = coeffs(SOLV);
        pp = length(c2);
        yy=yy-yy/2;
        if pp==0
            gap=ga;
            ga=ga+yy;
        end  
        if pp~=0
            gamma_final=ga;
            gap=ga;
            ga=ga-yy;
            Barrier_certificate=SOLV;
        end
end

% ===================================
% Printing final values for parameters and barrier certificate  
cc=c_final;
gg=gamma_final;
bb=Barrier_certificate;

% Output:
%
% c_final = 0.00012
% gamma_final = 0.002
% Barrier_certificate =
% 2.331e-11*x1^5 + 4.571e-7*x1^4*x2 + 4.116e-6*x1^4 + 1.417e-6*x1^3*x2^2 
% + 1.406e-5*x1^3*x2 + 1.448e-5*x1^3 + 1.593e-6*x1^2*x2^3 + 2.619e-5*x1^2*x2^2 
% + 4.211e-5*x1^2*x2 + 1.721e-5*x1^2 + 6.021e-7*x1*x2^4 + 1.638e-5*x1*x2^3 
% + 4.467e-5*x1*x2^2 + 3.527e-5*x1*x2 + 6.725e-6*x1 + 9.347e-12*x2^5 
% + 7.16e-6*x2^4 + 2.356e-5*x2^3 + 2.586e-5*x2^2 + 8.672e-6*x2 + 9.714e-7

% Elapsed time is 667.222063 seconds.

