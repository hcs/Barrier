% File demonstrate computation of barrier certificate and probability
% bounds using sum-of square programing for Example 3 in paper 
% "P. Jagtap, S. Soudjani, and M. Zamani, Temporal Logic Verification 
% of Stochastic Systems using Barrier Certificates, submitted to ATVA 2018
% 
% Example: Lorenz model for a thermal convection loop
%
% Requirements: to run this code user needs to install 
%       1] SOSTOOLS (download: http://www.cds.caltech.edu/sostools/) 
%       2] SeDuMi (download: http://sedumi.ie.lehigh.edu/)
% Created on: 3 May 2018
% Author: Pushpak Jagtap
% Tested on: MATLAB R2017a on Windows PC
%
% NOTE: One can easily modify generate_prog.m file for simulating other 
% examples in the paper.  
%
% following code do bisection method to compute papameters $gamma$ and $c$  
% in the SOS program which gives reachability probability as $gamma+N*c$ 
% =============================================
clc;
clear all;
tic
% =============================================
% Initial geuss for gamma and c (Selected from intiution to reduce
% computation time but one can always use 1)
ga=0.1; ca=0.01; 
gap=0; cap=0; zz=ca; yy=ga;  % temporary variables

% bisection for computing gamma and c
while((abs(ca-cap)>=0.001)) %0.001 is a precision (stopping criteria for bisecion)
        [prog, V] =generate_prog(ga,ca); 
        % =============================================
        % get solution
        SOLV = sosgetsol(prog,V);   
        % =============================================
        % checking if solution is feasible or not 
        [c2,m2] = coeffs(SOLV);
        pp = length(c2);
        zz=zz-zz/2;
        if pp==0
            cap=ca;
            ca=ca+zz;
        end
        if pp~=0
            c_final=ca;
            cap=ca;
            ca=ca-zz;
        end
end
ca=c_final;
while((abs(ga-gap)>=0.001)) %0.001 is a precision (stopping criteria for bisecion
        [prog, V] =generate_prog(ga,ca);   
        % =============================================
        % get solution
        SOLV = sosgetsol(prog,V); 
        % =============================================
        % checking if solution is feasible or not 
        [c2,m2] = coeffs(SOLV);
        pp = length(c2);
        yy=yy-yy/2;
        if pp==0
            gap=ga;
            ga=ga+yy;
        end  
        if pp~=0
            gamma_final=ga;
            gap=ga;
            ga=ga-yy;
            Barrier_certificate=SOLV;
        end
end
toc
% ===================================
% Printing final values for parameters and barrier certificate  
c_final
gamma_final
Barrier_certificate

% Output:
%
% c_final = 0.00125
% gamma_final = 0.0016
% Barrier_certificate =
% 1.397e-5*x1^4 - 1.529e-9*x1^3*x2 - 7.193e-9*x1^3*x3 + 1.8e-8*x1^3 
% + 1.7e-5*x1^2*x2^2 - 3.575e-10*x1^2*x2*x3 - 1.29e-10*x1^2*x2 
% + 1.828e-5*x1^2*x3^2 + 0.0001583*x1^2*x3 - 0.01397*x1^2 - 1.244e-9*x1*x2^3 
% + 9.497e-10*x1*x2^2*x3 - 4.268e-9*x1*x2^2 - 5.975e-8*x1*x2*x3^2 
% + 5.212e-8*x1*x2*x3 + 1.293e-7*x1*x2 + 1.558e-8*x1*x3^3 - 2.468e-9*x1*x3^2 
% - 6.421e-8*x1*x3 - 8.739e-8*x1 + 1.433e-5*x2^4 - 1.445e-9*x2^3*x3 
% + 5.404e-9*x2^3 + 1.805e-5*x2^2*x3^2 + 0.0001588*x2^2*x3 - 0.01398*x2^2 
% + 7.153e-9*x2*x3^3 - 1.278e-9*x2*x3^2 - 2.777e-8*x2*x3 - 5.848e-8*x2 
% + 1.318e-5*x3^4 + 7.945e-5*x3^3 + 0.06517*x3^2 - 0.8495*x3 + 4.151

% Elapsed time is 194.315589 seconds.

% ========================
% In this example, time horizon N=10 so the upper bound on probability is
% 0.0016+0.00125*10=0.0141

