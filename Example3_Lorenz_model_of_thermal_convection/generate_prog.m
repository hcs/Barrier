% File demonstrate computation of barrier certificate and probability
% bounds using sum-of square programing for Example 3 in paper 
% "P. Jagtap, S. Soudjani, and M. Zamani, Temporal Logic Verification 
% of Stochastic Systems using Barrier Certificates, submitted to ATVA 2018
% 
% Example: Lorenz model for a thermal convection loop
%
% Requirements: to run this code user needs to install 
%       1] SOSTOOLS (download: http://www.cds.caltech.edu/sostools/) 
%       2] SeDuMi (download: http://sedumi.ie.lehigh.edu/)
% Created on: 3 May 2018
% Author: Pushpak Jagtap
% Tested on: MATLAB R2017a on Windows PC
%
% NOTE: One can easily modify generate_prog.m file for simulating other 
% examples in the paper.  

function [y,z]=generate_prog(ga,ca)
        % =============================================
        % Define symbolic variables states and noise signals 
        syms x1 x2 x3 w1 w2 w3;
        
        % =============================================
        % Define variables required for SOS program 
        vars = [x1; x2; x3];
        
        % =============================================
        % Define Dynamics x(k+1)=f(x(k),w(k)) here
        f = [0.9*x1+0.1*x2+0.025*x1*w1;
            0.99*x2+0.01*x2*x3+0.025*x2*w2;
            1.0267*x3+0.01*x1*x2+0.025*x3*w3];

        % =============================================
        % Define regions of interest as sospolynomials 
        g0 = [(x1+10)*(10-x1); (x2+10)*(10-x2);(x3-2)*(10-x3)]; %p0
        g1 = [(x1+10)*(10-x1); (x2+10)*(10-x2);(x3+2)*(2-x3)]; %p1
        g2 = [(x1+10)*(10-x1); (x2+10)*(10-x2);(x3+10)*(-2-x3)]; %p2
        
        % =============================================
        %  Initialization the sum of squares program
        prog = sosprogram(vars);
        
        %  Defining monomials for barrier certificates and lagrange multiplier 
        VEC = monomials(vars,[0:4]);
        VEC1 = monomials(vars,[0]);
        
        % =============================================
        % The Barrier function V: 
        [prog,V] = sospolyvar(prog,VEC,'wscoeff');

        % =============================================
        % Computing E[V(x(k+1) | x(k)]
        VV = subs(V,{x1,x2,x3},[f(1),f(2),f(3)]);
        [c,m] = coeffs(VV);
        for k=1:length(m)
            s1 = strsplit(char(m(k)),'*');
            chk = 0;
            for k2=1:length(s1)
                s2 = strsplit(char(s1(k2)),'^');
                if (strcmp(s2(1),'w1')&& (length(s2)==1)) || (strcmp(s2(1),'w2')&& (length(s2)==1)) || (strcmp(s2(1),'w3')&& (length(s2)==1))
                    m(k) = 0 ;
                    chk = 1;
                    break
                end
                if (strcmp(s2(1),'w1') || strcmp(s2(1),'w2')|| strcmp(s2(1),'w3')) && mod(str2num(char(s2(2))),2)==1
                    m(k) = 0;
                    chk = 1;
                    break;
                end
                if (strcmp(s2(1),'w1') || strcmp(s2(1),'w2'))|| strcmp(s2(1),'w3')
                    dfk = str2num(char(s2(2)))/2;
                    df = (2^dfk)*factorial(dfk);
                    s1{k2} = num2str(df);
                end
            end
            if chk == 0
              sy1 = sym(s1);
              m(k) = prod(sy1);
            end
        end
        po2 = sum(c.*m);
        
        % =============================================
        % Lagrange multipliers
        [prog,V] = sospolyvar(prog,VEC,'wscoeff');
        [prog,V1] = sospolyvar(prog,VEC1,'wscoeff');
        [prog,V2] = sospolyvar(prog,VEC1,'wscoeff');
        [prog,V3] = sospolyvar(prog,VEC1,'wscoeff');
        [prog,V4] = sospolyvar(prog,VEC1,'wscoeff');
        [prog,V5] = sospolyvar(prog,VEC1,'wscoeff');
        [prog,V6] = sospolyvar(prog,VEC1,'wscoeff');

        % =============================================
        % Next, define SOSP constraints as mentioned in Lemma 3 in paper[1]

        prog = sosineq(prog,V);
        prog = sosineq(prog,-V-[V1 V2 V3]*g0+ga); 
        prog = sosineq(prog,V-[V4 V5 V6]*g2-1);  
        prog = sosineq(prog,-po2+V+ca);  
        


        % =============================================
        % And call sos solver (requires SeDuMi installed!)
        y = sossolve(prog);
        z=V;
end