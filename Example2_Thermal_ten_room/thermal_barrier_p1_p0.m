
function [cc,gg,bb]=thermal_barrier_p1_p0()
clear all;
c_final=0; gamma_final=0;
%==
% Initial geuss for gamma and c (Selected from intiution to reduce
% computation time but one can always use 1)
ga=0.1; ca=0.01; 
gap=0; cap=0; zz=ca; yy=ga;  % temporary variables

% bisection for computing gamma and c
while((abs(ca-cap)>=0.0001)) %0.001 is a precision (stopping criteria for bisecion)
        [prog, V] =generate_prog_thermal_2(ga,ca); 
        % =============================================
        % get solution
        SOLV = sosgetsol(prog,V);   
        % =============================================
        % checking if solution is feasible or not 
        [c2,m2] = coeffs(SOLV);
        pp = length(c2);
        zz=zz-zz/2;
        if pp==0
            cap=ca;
            ca=ca+zz;
        end
        if pp~=0
            c_final=ca;
            cap=ca;
            ca=ca-zz;
        end
end

ca=c_final;
while((abs(ga-gap)>=0.0001)) %0.001 is a precision (stopping criteria for bisecion
        [prog, V] =generate_prog_thermal_2(ga,ca);   
        % =============================================
        % get solution
        SOLV = sosgetsol(prog,V); 
        % =============================================
        % checking if solution is feasible or not 
        [c2,m2] = coeffs(SOLV);
        pp = length(c2);
        yy=yy-yy/2;
        if pp==0
            gap=ga;
            ga=ga+yy;
        end  
        if pp~=0
            gamma_final=ga;
            gap=ga;
            ga=ga-yy;
            Barrier_certificate=SOLV;
        end   
end


% ===================================
% Printing final values for parameters and barrier certificate  
cc=c_final;
gg=gamma_final;
bb=Barrier_certificate;

