% File demonstrate computation of barrier certificate and probability
% bounds using sum-of square programing for Example 2 in paper 
% "P. Jagtap, S. Soudjani, and M. Zamani, Temporal Logic Verification 
% of Stochastic Systems using Barrier Certificates, submitted to ATVA 2018
% 
% Example: Thermal model of ten-room building
%
% Requirements: to run this code user needs to install 
%       1] SOSTOOLS (download: http://www.cds.caltech.edu/sostools/) 
%       2] SeDuMi (download: http://sedumi.ie.lehigh.edu/)
% Created on: 3 May 2018
% Author: Pushpak Jagtap
% Tested on: MATLAB R2017a on Windows PC  

clc;
clear all;
tic;
% computation of barreir certificates
[c1,gamma1, barrier1]=thermal_barrier_p0_p1();
[c2,gamma2, barrier2]=thermal_barrier_p1_p0();

% computing lower bound on probabilities  
p = 1-((gamma1+9*c1)+(gamma2+9*c2))
toc;

% Output:
%
% c1 = 0.00016
% gamma1 = 0.0002
% barrier1 =
% 4.022e-9*x1^2 - 1.076e-9*x1*x2 - 8.219e-10*x1*x3 - 8.192e-10*x1*x4 
% - 6.296e-10*x1*x5 - 8.968e-10*x1*x6 - 8.98e-10*x1*x7 - 8.968e-10*x1*x8 
% - 8.98e-10*x1*x9 - 8.968e-10*x1*x10 + 3.723e-9*x2^2 - 8.896e-10*x2*x3 
% - 6.189e-10*x2*x4 - 5.845e-10*x2*x5 - 6.296e-10*x2*x6 - 1.076e-9*x2*x7 
% - 6.296e-10*x2*x8 - 1.076e-9*x2*x9 - 6.296e-10*x2*x10 + 3.972e-9*x3^2 
% - 1.298e-9*x3*x4 - 6.189e-10*x3*x5 - 8.192e-10*x3*x6 - 8.219e-10*x3*x7 
% - 8.192e-10*x3*x8 - 8.219e-10*x3*x9 - 8.192e-10*x3*x10 + 3.972e-9*x4^2 
% - 8.896e-10*x4*x5 - 8.219e-10*x4*x6 - 8.192e-10*x4*x7 - 8.219e-10*x4*x8 
% - 8.192e-10*x4*x9 - 8.219e-10*x4*x10 + 3.723e-9*x5^2 - 1.076e-9*x5*x6 
% - 6.296e-10*x5*x7 - 1.076e-9*x5*x8 - 6.296e-10*x5*x9 - 1.076e-9*x5*x10 
% + 4.022e-9*x6^2 - 8.968e-10*x6*x7 - 8.98e-10*x6*x8 - 8.968e-10*x6*x9 
% - 8.98e-10*x6*x10 + 4.022e-9*x7^2 - 8.968e-10*x7*x8 - 8.981e-10*x7*x9 
% - 8.967e-10*x7*x10 + 4.022e-9*x8^2 - 8.968e-10*x8*x9 - 8.98e-10*x8*x10 
% + 4.022e-9*x9^2 - 8.968e-10*x9*x10 + 4.022e-9*x10^2

% c2 = 0.00016
% gamma2 = 0.0018
% barrier2 =
% 7.779e-8*x1^2 - 4.016e-9*x1*x2 + 5.988e-10*x1*x3 + 2.276e-9*x1*x4 
% + 5.35e-10*x1*x5 + 1.762e-9*x1*x6 + 2.992e-9*x1*x7 + 1.762e-9*x1*x8 
% + 2.992e-9*x1*x9 + 1.762e-9*x1*x10 - 3.504e-6*x1 + 8.492e-8*x2^2 
% - 3.348e-9*x2*x3 - 1.778e-9*x2*x4 - 1.29e-10*x2*x5 + 5.349e-10*x2*x6 
% - 4.016e-9*x2*x7 + 5.348e-10*x2*x8 - 4.016e-9*x2*x9 + 5.349e-10*x2*x10 
% - 3.254e-6*x2 + 8.2e-8*x3^2 - 1.048e-8*x3*x4 - 1.778e-9*x3*x5 
% + 2.276e-9*x3*x6 + 5.989e-10*x3*x7 + 2.276e-9*x3*x8 + 5.988e-10*x3*x9 
% + 2.276e-9*x3*x10 - 3.315e-6*x3 + 8.2e-8*x4^2 - 3.348e-9*x4*x5 
% + 5.989e-10*x4*x6 + 2.276e-9*x4*x7 + 5.991e-10*x4*x8 + 2.276e-9*x4*x9 
% + 5.99e-10*x4*x10 - 3.315e-6*x4 + 8.492e-8*x5^2 - 4.016e-9*x5*x6 
% + 5.35e-10*x5*x7 - 4.016e-9*x5*x8 + 5.349e-10*x5*x9 - 4.016e-9*x5*x10 
% - 3.254e-6*x5 + 7.779e-8*x6^2 + 1.762e-9*x6*x7 + 2.992e-9*x6*x8 
% + 1.761e-9*x6*x9 + 2.993e-9*x6*x10 - 3.504e-6*x6 + 7.779e-8*x7^2 
% + 1.762e-9*x7*x8 + 2.992e-9*x7*x9 + 1.762e-9*x7*x10 - 3.504e-6*x7 
% + 7.779e-8*x8^2 + 1.762e-9*x8*x9 + 2.992e-9*x8*x10 - 3.504e-6*x8
% + 7.779e-8*x9^2 + 1.762e-9*x9*x10 - 3.504e-6*x9 + 7.779e-8*x10^2 
% - 3.504e-6*x10 + 0.0003596