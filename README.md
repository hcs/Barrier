Matlab code to demonstrate computation of barrier certificate and probability bounds using sum-of-square programing for Examples in paper: 
P. Jagtap, S. Soudjani, and M. Zamani, Temporal Logic Verification of Stochastic Systems using Barrier Certificates, accepted for publication in ATVA 2018.

Requirements: to run this code user needs to install 

    1] SOSTOOLS (download: http://www.cds.caltech.edu/sostools/) 
    
    2] SeDuMi (download: http://sedumi.ie.lehigh.edu/)
    
Tested on: MATLAB R2017a on Windows PC
